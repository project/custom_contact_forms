Custom Contact Forms

The module allows to site admins create customized contact forms. 
Contact forms also can be created by other modules using provided API.
Contact form title, default message, description text, 
message subject and body are fully customizable and can use different variables 

To install, place the entire module folder into your modules directory.
Go to administer -> site building -> modules and enable the Custom Contact Forms module.

To create new contact form or change settings for existen contact form go to Administer -> Site configuration -> Custom Contact forms.

You can set up permissions for each contact form in Administer -> User Management -> Access Control

Created contact forms are available at http://yourdomain.com/custom_contact_forms/<form delta>/<nid>
If <nid> (node id) is provided in the query string node variables will be available in the contact form    

Other modules can use API to create their own contact forms.
To define new contact form implement hook_contact_forms():

function example_module_custom_contact_forms()
{
	$forms = array();
	$form = new stdClass();
	$form->delta = 'example_form';
	$form->source = 'example_module';
	$form->title = t('Example contact form');
	$forms['example_form'] = $form;
	return $forms;
}

To modify contact form parameters after form submit implement 
hook_custom_contact_forms_parameters($contact_form, $user, $node, $form_values, &$parameters).


